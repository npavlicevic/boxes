<?php

require_once('BinDao.php');

class BinDaoFactory{
	public static function create($object=NULL){
		if(!$object){
			return new BinDao();
		} else {
			return new $object;
		}
	}
}

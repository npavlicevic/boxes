<?php

require_once('BinDaoFactory.php');

class Main{
	public function __construct($argv){
		call_user_func(array($this,$argv[1]),$argv);
	}

	public function addBin($argv){
		BinDaoFactory::create()->saveByParentId($argv[2],$argv[3]);
	}

	public function showBins($argv){
		print print_r(BinDaoFactory::create()->getBins(),true);
	}

	public function getBinChildren($argv){
		print print_r(BinDaoFactory::create()->getChildrenById($argv[2]),true);
	}

	public function deleteBin($argv){
		BinDaoFactory::create()->deleteById($argv[2]);
	}
}

new Main($argv);

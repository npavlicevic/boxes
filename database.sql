DROP DATABASE IF EXISTS bins;

CREATE DATABASE bins;

USE bins;

DROP TABLE IF EXISTS bins;

CREATE TABLE bins(
	id INT(11) NOT NULL AUTO_INCREMENT,
	overlap TEXT,
	characters_parent INT,
	characters_me INT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

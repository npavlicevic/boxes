<?php

require_once('BinVo.php');

class BinDao{
	protected $connect;

	public function __construct($host='localhost',$username='root',$password='srbijA123',$database='bins'){
		$this->connect=new mysqli($host,$username,$password,$database);
	}

	public function execute($sql){
		$res=$this->connect->query($sql) or die($this->connect->error);
		if($res->num_rows>0){
			for($i=0;$i<$res->num_rows;$i++){
				$row=$res->fetch_assoc();

				$binVo[$i]=new BinVo();

				$binVo[$i]->setId($row['id']);
				$binVo[$i]->setOverlap($row['overlap']);
				$binVo[$i]->setCharactersParent($row['characters_parent']);
				$binVo[$i]->setCharactersMe($row['characters_me']);
				$binVo[$i]->setName($row['name']);
			}
		}

		return $binVo;
	}

	public function getByBinId($binId){
		$sql='SELECT * FROM bins WHERE id='.$binId;
		
		return $this->execute($sql);
	}

	public function getBins(){
		$sql='SELECT * FROM bins ORDER BY overlap ASC';
		
		return $this->execute($sql);
	}

	public function getChildren($binVo){
		$sql='SELECT bin.* FROM bins AS bin,bins AS parent 
		WHERE bin.overlap LIKE 
		CONCAT(\'%\',SUBSTRING(parent.overlap,parent.characters_parent+1,
		parent.characters_me-parent.characters_parent),\'%\') 
		AND parent.id='.$binVo->getId().' 
		AND bin.id<>'.$binVo->getId();

		return $this->execute($sql); 
	}

	public function getChildrenById($binId){
		return $this->getChildren(new BinVo(
			$binId,
			NULL,
			NULL,
			NULL,
			NULL
		));
	}

	public function save($binParentVo,$binVo){
		$binVo->setOverlap($binParentVo->getOverlap());
		$binVo->setCharactersParent($binParentVo->getCharactersMe());

		$sql='INSERT INTO bins (overlap,characters_parent,characters_me,name) VALUES ('.
		'CONCAT(\''.$binVo->getOverlap().'\',CONCAT(\'(\',UNIX_TIMESTAMP(),\')\')),'.
		$binVo->getCharactersParent().','.
		$binVo->getCharactersParent().'+CHAR_LENGTH(CONCAT(\'(\',UNIX_TIMESTAMP(),\')\')),'.
		'\''.$binVo->getName().'\')';
		
		$this->connect->query($sql);

		return $this->connect->affected_rows;
	}

	public function saveByParentId($parentId,$myName){
		$parentVo=$this->getByBinId($parentId);

		return $this->save(array_shift($parentVo),new BinVo(
			NULL,
			NULL,
			NULL,
			NULL,
			$myName
		));
	}

	public function delete($binVo){
		$sql='LOCK TABLES bins WRITE';
		$this->connect->query($sql);
		$sql='SELECT @my_tag:=SUBSTRING(overlap,characters_parent+1,characters_me-characters_parent) FROM bins WHERE id='.$binVo->getId();
		$this->connect->query($sql);
		$sql='DELETE FROM bins WHERE overlap LIKE CONCAT(\'%\',@my_tag,\'%\')';
		$this->connect->query($sql);
		$sql='UNLOCK TABLES';
		$this->connect->query($sql);

		return $this->connect->affected_rows;
	}
	
	public function deleteById($binId){
		return $this->delete(new BinVo(
			$binId,
			NULL,
			NULL,
			NULL,
			NULL
		));
	}
}

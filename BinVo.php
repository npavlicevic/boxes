<?php

class BinVo{
	protected $id;
	protected $overlap;
	protected $charactersParent;
	protected $charactersMe;
	protected $name;

	public function __construct($id=NULL,$overlap=NULL,$charactersParent=NULL,$charactersMe=NULL,$name=NULL){
		$this->id=$id;
		$this->overlap=$overlap;
		$this->charactersParent=$charactersParent;
		$this->charactersMe=$charactersMe;
		$this->name=$name;
	}

	public function setId($id){
		$this->id=$id;
	}

	public function getId(){
		return $this->id;
	}

	public function setOverlap($overlap){
		$this->overlap=$overlap;
	}

	public function getOverlap(){
		return $this->overlap;
	}

	public function setCharactersParent($charactersParent){
		$this->charactersParent=$charactersParent;
	}

	public function getCharactersParent(){
		return $this->charactersParent;
	}

	public function setCharactersMe($charactersMe){
		$this->charactersMe=$charactersMe;
	}

	public function getCharactersMe(){
		return $this->charactersMe;
	}

	public function setName($name){
		$this->name=$name;
	}

	public function getName(){
		return $this->name;
	}
}
